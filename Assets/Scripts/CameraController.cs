﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform posicion;
    public float velocidad;
    private PlayerController playerController;
    // Start is called before the first frame update
    
    static PlayerController GetPlayer()
    {
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            return player.GetComponent<PlayerController>();
        }
        else
        {
            return null;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        posicion = GetComponent<Transform>();
        playerController = GetPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = posicion.position;
        pos.x += velocidad * Time.deltaTime;
        posicion.position = pos;
    }
    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerController.Automorision();
        }
    }


}
